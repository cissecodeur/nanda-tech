FROM node:alpine
ADD . /app/                                                
WORKDIR /app                                                   
RUN npm install

EXPOSE 3000
VOLUME /app/logs

CMD npm run start 

