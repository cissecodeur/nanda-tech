import React , { Component } from 'react';
import { NavLink } from 'react-router-dom'
import { Header } from './Header';


export class boutique extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(


<div>

     <Header/>


  <section className="inner-banner" id="home">
    <div className="inner-layer">
      <div className="container">
      </div>
    </div>
  </section>
  {/* //inner-banner */}
  {/* breadcrumb */}
  <div className="breadcrumb-w3pvt bg-light">
    <div className="container">
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <NavLink to="home">Accueil</NavLink>
          </li>
          <li className="breadcrumb-item" aria-current="page">Boutique</li>
        </ol>
      </nav>
    </div>
  </div>
  {/* //breadcrumb */}
  {/* blog */}
  <section className="blog pt-5">
    <div className="container py-md-2">
      <h2 className="heading mb-sm-5 mb-4"><strong>Nos articles <span className="fa fa-shopping-cart" /></strong></h2>
      <div className="row">
        <div className="col-lg-6 news-left mb-5">
          <div className="row">
            <div className="col-md-9 w3-w3pvt-news-img">
             <img src="images/articles/01-full.jpg" alt />
              <h4>Imprimante hp</h4>
                  <p  style = {{ color :"red"}}><strike>35000 Fcfa</strike> &nbsp;&nbsp;25.000 Fcfa</p>
            </div>
                 <br/>

            <div className="col-md-9 w3-w3pvt-news-img">
             <img src="images/articles/02-full.jpg" alt />
                   <h4>Ordinateur portable</h4>
                   <p  style = {{ color :"red"}}><strike>200.000 Fcfa</strike> &nbsp;&nbsp; 150.000 Fcfa</p>
            </div>
                <br/>

            <div className="col-md-9 w3-w3pvt-news-img">
             <img src="images/articles/03-full.jpg" alt />
                <h4>Point d'acces D-LINK</h4>
                <p  style = {{ color :"red"}}><strike>36000 Fcfa</strike> &nbsp;&nbsp;24.000 Fcfa</p>
            </div>

            <div className="col-md-9 w3-w3pvt-news-img">
             <img src="images/articles/04-full.jpg" alt />
              <h4>Ordinateur Bureau HP 290 G2 core I3</h4>
               <p  style = {{ color :"red"}}><strike>300.000 Fcfa</strike> &nbsp;&nbsp;240.000 Fcfa</p>
            </div>

          
          </div>        
        </div>
         
          
        
        
        
        
    <div className="col-lg-4 news-left mb-5">
          <div className="row">
      
            <div className="col-md-9 w3-w3pvt-news-img">
              <img src="images/articles/05-full.jpg" alt />
                     <h4>Climatiseur NASCO 2chvx</h4>
                      <p  style = {{ color :"red"}}><strike>315.000 Fcfa</strike> &nbsp;&nbsp; 210.000 Fcfa</p>
    
            </div>

            <div className="">
              <img src="images/articles/cartouches.jpg" alt />
              <div className="portfolio-caption">
                <h4>Ordinateur Bureau HP 290 G2 core I3</h4>
                <p  style = {{ color :"red"}}><strike>300.000 Fcfa</strike> &nbsp;&nbsp;240.000 Fcfa</p>
              </div>
            </div>

            <div className="">
              <img src="images/s1.jpg" alt />
              <div className="portfolio-caption">
                <h4>Ordinateur Bureau HP 290 G2 core I3</h4>
                <p  style = {{ color :"red"}}><strike>300.000 Fcfa</strike> &nbsp;&nbsp;240.000 Fcfa</p>
              </div>
            </div>

            <div className="">
              <img src="images/s1.jpg" alt />
              <div className="portfolio-caption">
                <h4>Ordinateur Bureau HP 290 G2 core I3</h4>
                <p  style = {{ color :"red"}}><strike>300.000 Fcfa</strike> &nbsp;&nbsp;240.000 Fcfa</p>
              </div>
            </div>

            <div className="">
              <img src="images/s1.jpg" alt />
              <div className="portfolio-caption">
                    <h4>Climatiseur NASCO 2chvx</h4>
                      <p  style = {{ color :"red"}}><strike>315.000 Fcfa</strike> &nbsp;&nbsp; 210.000 Fcfa</p>
              </div>
            </div>
          </div>
        </div>
        </div>
    </div>
  </section>
  {/* //blog */}
 
</div>
        )
    }
  }
