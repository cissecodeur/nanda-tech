import React , { Component } from 'react';
import { db , auth } from '../Firebase/firebase'
import { NavLink } from 'react-router-dom'
import { Header } from './Header';


export class contact extends Component {
    constructor(props){
        super(props);
           this.state = { 
                          nom : "" , email : "" , message : ""
                         }
    }

    handleSubmit = (event) =>{
       event.preventDefault();
       db.collection("contact_nanda")
       .add(
            {
              nom : this.state.nom,
              email : this.state.email,
              message : this.state.message
            })
       .then(()=>{
              alert  ("message envoyé avec succes")
          })
           .catch(error => alert("non envoyé"))
           
        this.setState({
                nom: "",
                email : "",
                message: ""
         })

    }
    

    handleChange = (event) =>{
         this.setState ({[event.target.name] : event.target.value })
                               }
    render(){
        return(

<div>

      <Header/>
  
  {/* header */}
  
  {/* //header */}
  {/* inner-banner */}
  <section className="inner-banner" id="home">
    <div className="inner-layer">
      <div className="container">
      </div>
    </div>
  </section>
  {/* //inner-banner */}
  {/* breadcrumb */}
  <div className="breadcrumb-w3pvt bg-light">
    <div className="container">
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <NavLink to ="home">Accueil</NavLink>
          </li>
          <li className="breadcrumb-item" aria-current="page">Contact</li>
        </ol>
      </nav>
    </div>
  </div>
  {/* //breadcrumb */}
  {/* contact */}
  <section className="contact py-5">
    <div className="container py-sm-5">
      <h2 className="heading mb-sm-5 mb-4">Nous contacter<strong>  </strong></h2>
      <div className="row">
        <div className="col-lg-4 col-md-6 contact-address">
          <h4 className="mb-sm-4 mb-3 w3f_title">Addresse</h4>
          <ul className="list-w3">
            <li className="d-flex"><span className="fa mt-1 mr-1 fa-map-marker" /> Deux plateaux Paillet </li>
            <li className="my-3"><span className="fa mr-1 fa-phone" />00225-78-77-39-60</li>
            <li className="my-3"><span className="fa mr-1 fa-phone" />00225-09-77-26-21</li>
            <li className><span className="fa mr-1 fa-envelope" /><a href ="mailto:infos@nanda.ci">infos@nanda.ci</a></li>
          </ul>
          <h4 className="mt-md-5 mt-4 mb-3 w3f_title">Nous suivre</h4>
          <p>Nous sommes également joignable sur les différents réseaux sociaux suivants :</p>
          <ul className="list-social">
            <li className="mr-2"><a href="https://www.facebook.com/nanda.abidjan" className="facebook"><span className="fa mr-1 fa-facebook" /></a></li>
            <li className="mr-2"><a href ="https://join.skype.com/invite/kN6TwK5g98t0" className="twitter"><span className="fa mr-1 fa-skype" /></a></li>
            <li className="mr-2"><a href="abidjannanda@gmail.com" className="google"><span className="fa mr-1 fa-google-plus" /></a></li>
          </ul>
        </div>
        <div className="col-lg-4 col-md-6 contact-form mt-md-0 mt-4">
          <h4 className="mb-sm-4 mb-3 w3f_title">Ecrivez-nous</h4>
          <form  onSubmit = { this.handleSubmit } name="contactform" id="contactform" noValidate="novalidate" required>
            <div className="form-group">
              <label>Nom</label>
              <input type="text" className="form-control" id="nom" value = { this.state.nom } placeholder="votre nom" name="nom" onChange = { this.handleChange } required />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input type="email" className="form-control" id="email" value = { this.state.email }  placeholder="votre Email" name="email" onChange = { this.handleChange } required/>
            </div>
            <div className="form-group">
              <label>Comment pouvons nous vous aider?</label>
              <textarea name="message" className="form-control" id="message" value = { this.state.message }  placeholder="Votre message" defaultValue={""}  onChange = { this.handleChange } required/>
            </div>				
            <button type="submit" className="btn btn-default">Envoyer</button>
          </form>
        </div>
        <div className="col-lg-4 mt-lg-0 mt-5 contact-map">
          <h4 className="mb-4 w3f_title">Geolocalisation de nos locaux</h4>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.9503398796587!2d-73.9940307!3d40.719109700000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a27e2f24131%3A0x64ffc98d24069f02!2sCANADA!5e0!3m2!1sen!2sin!4v1441710758555" allowFullScreen />
        </div>
      </div>
    </div>
  </section>
  {/* //contact */}
 
  {/* move top */}
  </div> )

        }
    }

