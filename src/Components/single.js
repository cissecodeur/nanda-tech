<div>
  {/*
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
*/}
  <title>Demand Corporate Category Flat Bootstrap Responsive Website Template | Single :: w3layouts</title>
  {/* for-mobile-apps */}
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="Demand Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
  {/* css files */}
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />{/* bootstrap css */}
  <link href="css/style.css" rel="stylesheet" type="text/css" />{/* custom css */}
  <link href="css/font-awesome.min.css" rel="stylesheet" />{/* fontawesome css */}
  {/* //css files */}
  {/* google fonts */}
  <link href="//fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet" />
  {/* //google fonts */}
  {/* header */}
 
  {/* //header */}
  {/* inner-banner */}
  <section className="inner-banner" id="home">
    <div className="inner-layer">
      <div className="container">
      </div>
    </div>
  </section>
  {/* //inner-banner */}
  {/* breadcrumb */}
  <div className="breadcrumb-w3pvt bg-light">
    <div className="container">
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <a href="index.html">Home</a>
          </li>
          <li className="breadcrumb-item" aria-current="page">Single</li>
        </ol>
      </nav>
    </div>
  </div>
  {/* //breadcrumb */}
  {/* single */}
  <section className="bottom-banner-w3layouts py-5">
    <div className="container py-md-3">
      <h2 className="heading mb-sm-5 mb-4">Single<strong> Page </strong></h2>
      <div className="row inner-sec-w3ls-w3pvtinfo">
        {/*left*/}
        <div className="col-lg-8 left-blog-info text-left">
          <div className="card">
            <a href="single.html">
              <img src="images/s1.jpg" className="card-img-top img-fluid rounded" alt />
            </a>
            <div className="card-body">
              <ul className="blog-icons my-sm-4 my-3">
                <li>
                  <a href="#">
                    <span className="fa fa-calendar" /> Feb 27 .2018</a>
                </li>
                <li className="mx-2">
                  <a href="#">
                    <span className="fa fa-comment" /> 21</a>
                </li>
                <li>
                  <a href="#">
                    <span className="fa fa-eye" /> 2000</a>
                </li>
              </ul>
              <h5 className="card-title ">
                <a href="single.html">Blog Post Title</a>
              </h5>
              <p className="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt,Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore et dolore magna aliqua uta enim ad minim ven iam quis nostrud exercitation ullamco labor nisi ut aliquip exea commodo consequat duis aute irudre dolor in elit sed uta labore dolore reprehender.. </p>
              <div className="read inner mt-4">
                <a href="single.html" className="btn btn-sm animated-button victoria-two">Read More</a>
              </div>
            </div>
          </div>
          <div className="comment-top">
            <h4>Comments</h4>
            <div className="media">
              <img src="images/team1.jpg" alt className="img-fluid rounded" />
              <div className="media-body">
                <h5 className="mt-sm-0 mt-1">Joseph Goh</h5>
                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. id dignissim sapien velit id felis ac cursus eros.
                  Cras a ornare elit.</p>
              </div>
            </div>
            <div className="media">
              <img src="images/team2.jpg" alt className="img-fluid rounded" />
              <div className="media-body">
                <h5 className="mt-sm-0 mt-1">Richard Spark</h5>
                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. id dignissim sapien velit id felis ac cursus eros.
                  Cras a ornare elit.</p>
              </div>
            </div>
          </div>
          <div className="comment-top">
            <h4>Leave a Comment</h4>
            <div className="comment-bottom">
              <form action="#" method="post">
                <input className="form-control" type="text" name="Name" placeholder="Name" required />
                <input className="form-control" type="email" name="Email" placeholder="Email" required />
                <input className="form-control" type="text" name="Subject" placeholder="Subject" required />
                <textarea className="form-control" name="Message" placeholder="Message..." required defaultValue={""} />
                <button type="submit" className="btn btn-primary submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
        {/*//left*/}
        {/*right*/}
        <aside className="col-lg-4 right-blog-con text-left">
          <div className="right-blog-info text-left">
            <div className="tech-btm">
              <img src="images/s2.jpg" className="card-img-top img-fluid rounded" alt />
            </div>
            <div className="tech-btm">
              <h4>Sign up to our newsletter</h4>
              <p>Pellentesque dui, non felis. Maecenas male </p>
              <form action="#" method="post">
                <input className="form-control" type="email" placeholder="Email" required />
                <button className="form-control btn" type="submit">Subscribe</button>
              </form>
            </div>
            <div className="tech-btm">
              <h4>Categories</h4>
              <ul className="list-group single">
                <li className="list-group-item d-flex justify-content-between align-items-center">
                  Cras justo odio
                  <span className="badge badge-primary badge-pill">14</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center">
                  Dapibus ac facilisis in
                  <span className="badge badge-primary badge-pill">2</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center">
                  Morbi leo risus
                  <span className="badge badge-primary badge-pill">1</span>
                </li>
              </ul>
            </div>
            <div className="tech-btm">
              <h4>Top stories of the week</h4>
              <div className="blog-grids row mb-3">
                <div className="col-md-5 blog-grid-left">
                  <a href="single.html">
                    <img src="images/s2.jpg" className="card-img-top img-fluid rounded" alt />
                  </a>
                </div>
                <div className="col-md-7 blog-grid-right">
                  <h5>
                    <a href="single.html">Pellentesque dui, non felis. Maecenas male non felis </a>
                  </h5>
                  <div className="sub-meta">
                    <span>
                      <span className="fa fa-clock-o" /> 27 Feb, 2018</span>
                  </div>
                </div>
              </div>
              <div className="blog-grids row mb-3">
                <div className="col-md-5 blog-grid-left">
                  <a href="single.html">
                    <img src="images/s3.jpg" className="card-img-top img-fluid rounded" alt />
                  </a>
                </div>
                <div className="col-md-7 blog-grid-right">
                  <h5>
                    <a href="single.html">Pellentesque dui, non felis. Maecenas male non felis </a>
                  </h5>
                  <div className="sub-meta">
                    <span>
                      <span className="fa fa-clock-o" /> 27 Feb, 2018</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="tech-btm widget_social text-left">
              <h4>Stay Connect</h4>
              <ul>
                <li>
                  <a className="twitter" href="#">
                    <span className="fa fa-twitter" />
                    <span className="count">317K</span> Twitter Followers</a>
                </li>
                <li>
                  <a className="facebook" href="#">
                    <span className="fa fa-facebook" />
                    <span className="count">218k</span> Facebook Followers</a>
                </li>
                <li>
                  <a className="dribble" href="#">
                    <span className="fa fa-dribbble" />
                    <span className="count">215k</span> Dribble Followers</a>
                </li>
                <li>
                  <a className="pin" href="#">
                    <span className="fa fa-pinterest" />
                    <span className="count">190k</span> Pinterest Followers</a>
                </li>
              </ul>
            </div>
            <div className="tech-btm">
              <h4>Recent Posts</h4>
              <div className="blog-grids row mb-3 text-left">
                <div className="col-md-5 blog-grid-left">
                  <a href="single.html">
                    <img src="images/s3.jpg" className="card-img-top img-fluid rounded" alt />
                  </a>
                </div>
                <div className="col-md-7 blog-grid-right">
                  <h5>
                    <a href="single.html">Pellentesque dui, non felis. Maecenas male non felis </a>
                  </h5>
                  <div className="sub-meta">
                    <span>
                      <span className="fa fa-clock-o" /> 27 Feb, 2018</span>
                  </div>
                </div>
              </div>
              <div className="blog-grids row mb-3 text-left">
                <div className="col-md-5 blog-grid-left">
                  <a href="single.html">
                    <img src="images/s2.jpg" className="card-img-top img-fluid rounded" alt />
                  </a>
                </div>
                <div className="col-md-7 blog-grid-right">
                  <h5>
                    <a href="single.html">Pellentesque dui, non felis. Maecenas male non felis </a>
                  </h5>
                  <div className="sub-meta">
                    <span>
                      <span className="fa fa-clock-o" /> 27 Feb, 2018</span>
                  </div>
                </div>
                <div className="clearfix"> </div>
              </div>
              <div className="blog-grids row mb-3 text-left">
                <div className="col-md-5 blog-grid-left">
                  <a href="single.html">
                    <img src="images/s3.jpg" className="card-img-top img-fluid rounded" alt />
                  </a>
                </div>
                <div className="col-md-7 blog-grid-right">
                  <h5>
                    <a href="single.html">Pellentesque dui, non felis. Maecenas male non felis </a>
                  </h5>
                  <div className="sub-meta">
                    <span>
                      <span className="fa fa-clock-o" /> 27 Feb, 2018</span>
                  </div>
                </div>
                <div className="clearfix"> </div>
              </div>
            </div>
          </div>
        </aside>
        {/*//right*/}
      </div>
    </div>
  </section>
  {/* //Single */}

  {/* //footer */}
  {/* copyright */}
  <section className="copy-right py-4">
    <div className="container">
      <div className="row">
        <div className="col-lg-7">
          <p className>© 2019 Demand. All rights reserved | Design by
            <a href="http://w3layouts.com"> W3layouts.</a>
          </p>
        </div>
        <div className="col-lg-5">
          <ul className="list-w3 d-sm-flex">
            <li>
              <a href="#">
                Privicy Policy
              </a>
            </li>
            <li className="mx-4">
              <a href="#">
                Terms &amp; Conditions
              </a>
            </li>
            <li>
              <a href="#">
                Disclaimer.
              </a>
            </li>
            <li>
              <a href="#">
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  {/* //copyright */}
  {/* move top */}
  <div className="move-top text-right">
    <a href="#home" className="move-top"> 
      <span className="fa fa-angle-up  mb-3" aria-hidden="true" />
    </a>
  </div>
  {/* move top */}
</div>
