import React , { Component } from 'react';
import { NavLink } from 'react-router-dom'
import { Header } from './Header';
import { db , auth } from '../Firebase/firebase'

export class about extends Component {
	constructor(props){
    super(props);
    this.state = { email : "" }
  }
  
  handleSubmit = (event) =>{
    event.preventDefault();
    db.collection("newsletter")
    .add(
         {
           email : this.state.email,          
         })
         .then(()=>{
             alert("message envoyé avec succes")
         })
          .catch(error => alert(error))
      this.setState({
             email : ""            
      })

 }

 handleChange = (event) =>{
  this.setState ({[event.target.name] : event.target.value })
                        }

	render(){
		return(
			<div>

        <Header />

  {/* //header */}
  {/* inner-banner */}
  <section className="inner-banner" id="home">
    <div className="inner-layer">
      <div className="container">
      </div>
    </div>
  </section>
  {/* //inner-banner */}
  {/* breadcrumb */}
  <div className="breadcrumb-w3pvt bg-light">
    <div className="container">
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <NavLink to="home">Accueil</NavLink>
          </li>
          <li className="breadcrumb-item" aria-current="page">Qui sommes nous ?</li>
        </ol>
      </nav>
    </div>
  </div>
  {/* //breadcrumb */}
  {/* advantages and details */}
  <section className="advantages pt-5">
    <div className="container pb-md-5">
      <div className="row advantages_grids">
        <div className="col-lg-6 ">
          <img src="images/about.jpeg" alt className="img-fluid card-border" />
        </div>
        <div className="col-lg-6 mt-lg-0 mt-4">
          <h3 className="mt-sm-3">Une équipe de jeune informaticiens ivoiriens dynamiques et experimentés</h3>
          <p className="my-sm-4 mt-2 mb-4">  Nous sommes Spécialisés dans le Développement de Sites Et D'applications Web, Dans L'infogérance , Dans L'hébergement Et La Location De Serveurs. Nous Vous Proposons Également La Vente Et La Maintenance De Matériels Informatique.
n'hésitez pas a nous <NavLink to="contact">contacter</NavLink> pour plus d'informations.</p>
        </div>
        <div className="row mt-sm-5 mx-0">
          <div className="col-lg-3 col-6 grid4 mb-5">
            <span className="fa fa-server" />
            <h3 className="mt-sm-3 mt-2">Déploiement serveurs</h3>
            <p className> Votre entreprise se développe, vous nécessitez une puissance de calcul accrue.
            Vous souhaitez sauvegarder tous vos postes en interne.
            <strong>Nanda Technologie</strong> installe votre salle serveur ou modifie votre salle existante.</p>
          </div>
          <div className="col-lg-3 col-6 grid5 mb-5">
            <span className="fa fa-book" />
            <h3 className="mt-sm-3 mt-2">Formations</h3>
            <p className> Traitement de texte, Création d’un Site Web, Hebergement de votre applications web,
                        Administrations de serveurs Windows et Linux.</p>
            <p><strong>Apprenez avec des professionnels !</strong></p>
          </div>
          <div className="col-lg-3 col-6 grid6 mb-5">
            <span className="fa fa-cogs" />
            <h3 className="mt-sm-3 mt-2">Depannages </h3>
          <p className>Optimisation de votre ordinateur.
                      Suppression des Virus et de logiciels indésirables.
                      Sauvegarde des données.
                      Changement/Ajout de pièces.</p>
          <p><strong>Travaillez dans des conditions optimales !</strong></p>
          </div>
          <div className="col-lg-3 col-6 grid7 mb-5">
            <span className="fa fa-code" />
            <h3 className="mt-sm-3 mt-2">Developpement web</h3>
          <p className>Nous créons votre application, site Vitrine ou E-commerce, sur mesure. 
                      Avec un résultat de qualité professionnelle et un budget maitrisé .</p>
          <p><strong>Offrez-vous une vitrine ouverte sur le monde !</strong></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  {/* //advantages and details */}
  {/* testimonials */}
  <section className="clients">
    <div className="layer py-lg-5 py-lg-4">
      <div className="container py-5">
        <h2 className="heading mb-md-5 mb-4">ce que pensent <strong>nos clients</strong></h2>
        <div className="row">
          <div className="col-lg-4 col-md-6 pl-sm-0 mb-4">
            <div className="col- client-grid card-border ">
              <div className="c-left">
                <img src="images/client1.jpg" alt="image" className="img-fluid" />
                <div className="info">
                  <h6>Kouamé Stanislas</h6>
                  <p>- Client</p>
                </div>
                <div className="clearfix" />
              </div>
              <p className="mt-2">Service de qualité,personnels tres reactifs et competents.</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 pl-sm-0 mb-4">
            <div className="col- client-grid card-border">
              <div className="c-left">
                <img src="images/client2.jpeg.jpeg" alt="image" className="img-fluid" />
                <div className="info">
                  <h6>Ouattara Namory F.</h6>
                  <p>- Client </p>
                </div>
                <div className="clearfix" />
              </div>
              <p className="mt-2">.Equipes de personnes experimentées,j'ai ete satisfait de l'installation de ma salle Serveurs</p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 pl-sm-0 mb-4">
            <div className="col- client-grid card-border">
              <div className="c-left">
                <img src="images/client3.jpg" alt="image" className="img-fluid" />
                <div className="info">
                  <h6>Aude Irie </h6>
                  <p>- Client</p>
                </div>
                <div className="clearfix" />
              </div>
              <p className="mt-2">Satisfaite de la prestattions.Mais,personnels pas facilement joingnable,veuillez voir ce coté.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  {/* //testimonials */}
  {/* directors */}
  
  {/* //directors */}
  {/* subscribe */}
  <div className="subscribe py-5">
    <div className="container py-sm-4">
      <h3 className="heading mb-md-5 mb-4">souscrire au <strong> newsletter</strong></h3>
      <div className="row">
        <div className="col-md-6 w3-w3pvt-subscribe">
          <form control onSubmit = { this.handleSubmit } >
            <input value = { this.state.email } id="email" type="email"  onChange = { this.handleChange } placeholder="Entrer votre email ici" required />
            <button className="btn1"><span className="fa fa-paper-plane" aria-hidden="true" /></button>
          </form>
          <p>Nous ne divulguerons jamais vos emails.</p>
        </div>
        <div className="col-md-6 mt-md-0 mt-4">
          <h4>Inscrivez vous a nos newsletter pour etre informé de nos offres professionnels.</h4>
        </div>
      </div>
    </div>
  </div>
 
</div>
		)
	}

}
