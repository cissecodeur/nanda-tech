import React , { Component } from 'react';
import { NavLink  } from 'react-router-dom'
import { Header } from './Header';

export class services extends Component {
   constructor(props){
     super(props);
   }

   render(){
     return(
      <div>
       <Header/>
      
      {/* inner-banner */}
      <section className="inner-banner" id="home">
        <div className="inner-layer">
          <div className="container">
          </div>
        </div>
      </section>
      {/* //inner-banner */}
      {/* breadcrumb */}
      <div className="breadcrumb-w3pvt bg-light">
        <div className="container">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <NavLink to="home">Accueil</NavLink>
              </li>
              <li className="breadcrumb-item" aria-current="page">Services</li>
            </ol>
          </nav>
        </div>
      </div>
      {/* //breadcrumb */}
      {/* services */}
      <section className="services py-5">
        <div className="container py-lg-5 py-sm-3">
          <h2 className="heading mb-5"> <strong> Les services que nous fournissons </strong></h2>
          <div className="row">
            <div className="col-lg-4">
              <div className="row">
                <div className="col-9 mt-lg-5 ">
                  <h3>Developpement d'applications web</h3>
                  <p className="mt-2"> Nous créons votre application, site Vitrine ou E-commerce, sur mesure. 
                      Avec un résultat de qualité professionnelle et un budget maitrisé .</p>
                    <p><strong>Offrez-vous une vitrine ouverte sur le monde !</strong></p>
                </div>
                <div className="col-3 mt-lg-5 icon text-center grid1">
                  <span className="fa fa-code " />
                </div>
                <div className="col-9  mt-5">
                  <h3>Depannages informatiques</h3>
                  <p className="mt-2"> Optimisation de votre ordinateur.
                      Suppression des Virus et de logiciels indésirables.
                      Sauvegarde des données.
                      Changement/Ajout de pièces.</p>
                      <p><strong>Travaillez dans des conditions optimales!</strong></p>
                </div>
                <div className="col-3 mt-sm-5 mt-4 icon text-center grid4">
                  <span className="fa fa-cogs" />
                </div>
                <div className="col-9  mt-5">
                  <h3>Formations informatiques</h3>
                  <p className="mt-2"> Traitement de texte, Création d’un Site Web, Hebergement de votre applications web,
                        Administrations de serveurs Windows et Linux.</p>
                      <p><strong>Apprenez avec des professionnels !</strong></p>
                </div>
                <div className="col-3 mt-sm-5 mt-4 icon text-center grid5">
                  <span className="fa fa-book" />
                </div>
              </div>
            </div>
            <div className="col-lg-4 text-lg-center mt-lg-0 mt-4">
              <img src="images/serv.jpg" alt className="img-fluid card-border" />
            </div>
            <div className="col-lg-4 text-right">
              <div className="row">
                <div className="col-3 mt-sm-5 mt-4 icon text-center grid6">
                  <span className="fa fa-shopping-cart" />
                </div>
               
                  <h3>Ventes de materiels informatiques</h3>
                  <p className="mt-2"> Ordinateurs, Imprimantes,
                   Serveurs NAS, Souris, Claviers, Disques Durs, Disques SSD, Mémoire RAM, Clefs USB...</p>
                <p>Pour plus d'infos 
                  veuillez visiter notre <a href = "https://boutique.nanda.ci">boutique </a> .
                </p>


                <div className="col-3 mt-sm-5 mt-4 icon text-center grid7">
                  <span className="fa fa-mobile" />
                </div>
                
                  <h3>Ventes de Telephones portables et accessoires</h3>
                  <p className="mt-2">iphone X,Xmas,Samsung S10,Huawei P40,Infinix,Itel,Techno. </p>
              
                <p>Pour plus d'infos 
                  veuillez visiter notre <a href = "https://boutique.nanda.ci">boutique</a> .
                </p> 
                <div className="col-3 mt-sm-5 mt-4 icon text-center grid3">
                  <span className="fa fa-headphones" />
                </div>
               
                  <h3>Disponible en presentiel ou sur teamviewer 24/7 </h3>
                  <p className="mt-2"> Pour nos depannages,nous nous deplaçons vers vous ou 
                  nous pouvons depanner votre ordianateur a distance avec <a href = "https://www.teamviewer.com/fr/">Teamviewer</a>.
                  </p>
    
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* //services */}
      {/* other services */}

      {/* move top */}
    </div>
    
     )
   }
}