import React , { Component } from 'react';

export class Modals extends Component {

        render(){
            return(
     <div>
  {/* Modal 1 */}
  <div className="portfolio-modal modal fade" id="portfolioModal1" tabIndex={-1} role="dialog" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="close-modal" data-dismiss="modal">
          <div className="lr">
            <div className="rl" />
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <div className="modal-body">
                {/* Project Details Go Here */}
                <h2 className="text-uppercase">IMPRIMANTE HP 2130</h2>
                <p className="item-intro text-muted">Imprimante hp multi-fonction couleur avec une capacité de tirage estimée a 2000 pages .</p>
                <img className="img-fluid d-block mx-auto" src="img/portfolio/01-full.jpg" alt />
                 <p>Photocopie, Scan , Impression , Cartouche couleur/noir 61A</p>
                <ul className="list-inline">
                  <li><strong>Date:</strong> Novembre 2019</li>
                  <li><strong>Client:</strong> grand public</li>
                  <li><strong>Categorie:</strong> Materiel Informatique</li>
                </ul>
                <button className="btn btn-danger" data-dismiss="modal" type="button">
                  <i className="" />
                  Fermer</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* Modal 2 */}
  <div className="portfolio-modal modal fade" id="portfolioModal2" tabIndex={-1} role="dialog" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="close-modal" data-dismiss="modal">
          <div className="lr">
            <div className="rl" />
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <div className="modal-body">
                {/* Project Details Go Here */}
                <h2 className="text-uppercase">ORDINATEUR PORTABLE</h2>
                <p className="item-intro text-muted">Pc portable ASUS Dual core pour tout types de travaux </p>
                <img className="img-fluid d-block mx-auto" src="img/portfolio/02-full.jpg" alt />
                <p>Ram 4GO, Disque dur 500 Go , Couleur jaune doré (Or) , Windows 10</p>
                <ul className="list-inline">
                  <li><strong>Date:</strong> 20 Novembre 2019</li>
                  <li><strong>Client:</strong>grand public</li>
                  <li><strong>Categorie:</strong> materiel Informatique</li>
                </ul>
                <button className="btn btn-danger" data-dismiss="modal" type="button">
                  <i className="" />
                  Fermer</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* Modal 3 */}
  <div className="portfolio-modal modal fade" id="portfolioModal3" tabIndex={-1} role="dialog" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="close-modal" data-dismiss="modal">
          <div className="lr">
            <div className="rl" />
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <div className="modal-body">
                {/* Project Details Go Here */}
                <h2 className="text-uppercase">POINT D'ACCES D-LINK</h2>
                <p className="item-intro text-muted">Pour etendre le wifi dans les zone eloignées de votre Box .</p>
                <img className="img-fluid d-block mx-auto" src="img/portfolio/03-full.jpg" alt />
                <p>Haut debit 300Mb/s , couleur blanche , frequence 5Ghz</p>
                <ul className="list-inline">
                  <li><strong>Date: </strong> 20 Novembre 2019</li>
                  <li><strong>Client: </strong> grand public</li>
                  <li><strong>Categorie: </strong> materiel Informatique</li>
                </ul>
                <button className="btn btn-danger" data-dismiss="modal" type="button">
                  <i className="" />
                  Fermer</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* Modal 4 */}
  <div className="portfolio-modal modal fade" id="portfolioModal4" tabIndex={-1} role="dialog" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="close-modal" data-dismiss="modal">
          <div className="lr">
            <div className="rl" />
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <div className="modal-body">
                {/* Project Details Go Here */}
                <h2 className="text-uppercase">ORDINATEUR BUREAU HP 290 G2</h2>
                <p className="item-intro text-muted">ordinateur de bureau core i3 pour tout types de travaux .</p>
                <img className="img-fluid d-block mx-auto" src="img/portfolio/04-full.jpg" alt />
                <p> Ram 4GO, Disque dur 500 Go , Couleur noire , ecran 20" Windows 10</p>
                <ul className="list-inline">
                  <li><strong>Date: </strong> 20 Novembre 2019</li>
                  <li><strong>Client: </strong>grand public</li>
                  <li><strong>Categorie: </strong> materiel Informatique</li>
                </ul>
                <button className="btn btn-danger" data-dismiss="modal" type="button">
                  <i className="" />
                  Fermer</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* Modal 5 */}
  <div className="portfolio-modal modal fade" id="portfolioModal5" tabIndex={-1} role="dialog" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="close-modal" data-dismiss="modal">
          <div className="lr">
            <div className="rl" />
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <div className="modal-body">
                {/* Project Details Go Here */}
                <h2 className="text-uppercase">CLIMATISEUR NASCO</h2>
                <p className="item-intro text-muted">Rafraichissez vos locaux avec ce climatiseur Nasco 2chvx</p>
                <img className="img-fluid d-block mx-auto" src="img/portfolio/05-full.jpg" alt />
                <p> adipisicing elit. reiciendis facere nemo!</p>
                <ul className="list-inline">
                  <li><strong>Date: </strong> 20 Novembre 2019</li>
                  <li><strong>Client: </strong> grand public</li>
                  <li><strong>Categorie: </strong> materiel Domotique</li>
                </ul>
                <button className="btn btn-danger" data-dismiss="modal" type="button">
                  <i className="" />
                  Fermer</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* Modal 6 */}
  <div className="portfolio-modal modal fade" id="portfolioModal6" tabIndex={-1} role="dialog" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="close-modal" data-dismiss="modal">
          <div className="lr">
            <div className="rl" />
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <div className="modal-body">
                {/* Project Details Go Here */}
                <h2 className="text-uppercase">KASPERSKY INTERNET SECURITY</h2>
                <p className="item-intro text-muted">Protegez vos ordinateurs et vos serveurs contre les virus,les vers et rançongiciels</p>
                <img className="img-fluid d-block mx-auto" src="img/portfolio/06-full.jpg" alt />
                <p>  maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                <ul className="list-inline">
                  <li><strong>Date: </strong> 20 Novembre 2019</li>
                  <li><strong>Client: </strong> grand public</li>
                  <li><strong>Categorie: </strong> materiel Informatique</li>
                </ul>
                <button className="btn btn-danger" data-dismiss="modal" type="button">
                  <i className="" />
                  Fermer</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

            )
        }
}