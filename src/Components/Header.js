import React ,  { Component }from 'react'
import { NavLink } from 'react-router-dom'

export class Header extends Component {
      constructor(props){
          super(props);

      }

      render(){
          return(
            
<div>
   {/*
    author: R4y
     
    */}
    <title></title>
      {/* for-mobile-apps */}
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="Demand Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

      {/* css files */}
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />{/* bootstrap css */}
      <link href="css/style.css" rel="stylesheet" type="text/css" />{/* custom css */}
      <link href="css/font-awesome.min.css" rel="stylesheet" />{/* fontawesome css */}
      
      {/* header */}
  {/* header */}
  <header>
    <div className="container">
      {/* nav */}
      <nav className="py-3 d-lg-flex">
        <div id="logo">
          <h1> <NavLink to ="home"><span className="" /> Nanda Tech </NavLink></h1>
        </div>
        <label htmlFor="drop" className="toggle"><span className="fa fa-bars" /></label>
        <input type="checkbox" id="drop" />
        <ul className="menu ml-auto mt-1">
          <li className="active"><NavLink to="/">Accueil</NavLink></li>
          <li className = "active"><NavLink to="about">Qui sommes nous ?</NavLink></li>
          <li className = "active"><NavLink to ="services">Nos services</NavLink></li>
          <li className = "active"><NavLink to="contact">Contact</NavLink></li>
        </ul>
        <li className="btn btn-primary"><NavLink to="contact">Demandez un devis !</NavLink></li>
      </nav>
      {/* //nav */}
    </div>
  </header>
  {/* //header */}
</div>

          )
      }
}


