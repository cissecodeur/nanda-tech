import React , { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Header } from './Header';


export class home extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
<div>
    
 <Header />
  
  {/* banner */}
  <div className="banner" id="home">
    <div className="layer">
      <div className="container">
        <div className="banner-text-w3pvt">
          {/* banner slider*/}
          <div className="csslider infinity" id="slider1">
            <input type="radio" name="slides" defaultChecked="checked" id="slides_1" />
            <input type="radio" name="slides" id="slides_2" />
            <input type="radio" name="slides" id="slides_3" />
            <ul className="banner_slide_bg">
              <li>
                <div className="w3ls_banner_txt">
                  <h2 className="b-w3ltxt text-capitalize mt-md-4">Nanda <span>Technologie</span> </h2>
                  <h4 className="b-w3ltxt text-capitalize">Entreprise de prestations de services informatique</h4>
                  <p className="w3ls_pvt-title my-3">Nous sommes spécialisés dans le développement de sites et d'applications web,de maintenances et de vente de materiels informatique. Nous sommes disponibles 24/7 et nous travaillons aussi bien avec les entreprises que les particuliers ( a domicile ). 
                    <br />  N'hésitez pas à nous contacter afin que nous commencions ensemble de nouveaux projets.
                    </p>
                  <NavLink to="services" className="btn btn-banner my-sm-3 mr-2">Plus d'infos</NavLink>
                  <NavLink to="contact" className="btn btn-banner1 my-sm-3">Demander un devis</NavLink>
                </div>
              </li>
              
             
            </ul>
          </div>
          {/* //banner slider*/}
        </div>
      </div>
    </div>
  </div>
  {/* //banner */}
  {/* about */}
  <section className="banner-bottom pt-sm-5 pt-4 mt-sm-0 mt-2">
    <div className="container">
      <div className="row bottom_grids text-center mt-lg-5 mt-3">
        <div className="col-md-4 grid1 mb-sm-5 mb-4">
          <span className="fa fa-code" />
          <h3 className="my-sm-3 mt-3 mb-2">Developpement d'applications web</h3>
          <p className>Nous créons votre application, site Vitrine ou E-commerce, sur mesure. 
                      Avec un résultat de qualité professionnelle et un budget maitrisé .</p>
          <p><strong>Offrez-vous une vitrine ouverte sur le monde !</strong></p>
        </div>
        <div className="col-md-4 grid2 mb-sm-5 mb-4">
          <span className="fa fa-cogs" />
          <h3 className="my-sm-3 mt-3 mb-2">Depannages informatiques</h3>
          <p className>Optimisation de votre ordinateur.
                      Suppression des Virus et de logiciels indésirables.
                      Sauvegarde des données.
                      Changement/Ajout de pièces.</p>
          <p><strong>Travaillez dans des conditions optimales!</strong></p>
        </div>
        <div className="col-md-4 grid3 mb-sm-5 mb-4">
          <span className="fa fa-book" />
          <h3 className="my-sm-3 mt-3 mb-2">Formations informatiques</h3>
          <p className> Traitement de texte, Création d’un Site Web, Hebergement de votre applications web,
                        Administrations de serveurs Windows et Linux.</p>
            <p><strong>Apprenez avec des professionnels !</strong>
          </p>
        </div>
      </div>
    </div>
  </section>
  
 
  <section className="products py-5">
    <div className="container py-lg-5 py-3">
      <h3 className="heading mb-md-5 mb-4"> Nos partenaires </h3>
      <div className="row products_grids text-center">
      <div className="col-md-3 col-6 grid5 mb-md-0 mb-4">
          
          <a href="#">
            <img src = "images/icm.jpeg" />
          </a>
      </div>
        <div className="col-md-3 col-6 grid5 mb-md-0 mb-4">
          
            <a href="#">
              <img src = "images/topexi.jpeg" />
            </a>
        </div>
        <div className="col-md-3 col-6 grid6">
          <div className="prodct1">
            <a href="#">
              <span className="fa fa-unlock" />
              <h3 className="mt-2">IT Software</h3>
              <span className="fas fa-long-arrow-alt-right" />
            </a>
          </div>
        </div>
        <div className="col-md-3 col-6 grid7">
          <div className="prodct1">
            <a href="#">
              <span className="fa fa-product-hunt" />
              <h3 className="mt-2">Cloud Technology</h3>
              <span className="fas fa-long-arrow-alt-right" />
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  {/* //products */}
  
</div>

        )
    }
}