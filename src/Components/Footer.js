import React , { Component } from 'react';
import {  NavLink } from 'react-router-dom'
import { Header } from './Header';



export class Footer extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div>
    <header>
    <div className="container">
      {/* nav */}
      <nav className="py-3 d-lg-flex">
        <div id="logo">
          <h1> <NavLink to ="home"><span className="" /> Nanda Tech </NavLink></h1>
        </div>
        <label htmlFor="drop" className="toggle"><span className="fa fa-bars" /></label>
        <input type="checkbox" id="drop" />
        <ul className="menu ml-auto mt-1">
          <li className="active"><NavLink to="/">Accueil</NavLink></li>
          <li className = "active"><NavLink to="about">Qui sommes nous ?</NavLink></li>
          <li className = "active"><NavLink to ="services">Nos services</NavLink></li>
          <li className = "active"><NavLink to="contact">Contact</NavLink></li>
        </ul>
        <li className="btn btn-primary"><NavLink to="contact">Demandez un devis !</NavLink></li>
      </nav>
      {/* //nav */}
    </div>
  </header>
             {/* footer */}
  <footer className="py-3 d-lg-flex">
    <div className="container pt-3">
      <div className="py-3 d-lg-flex">
       
     
      </div>
    </div>
    {/* //footer bottom */}
  </footer>
  {/* //footer */}
  {/* copyright */}
  <section className="copy-right py-4">
    <div className="container">
      <div className="row">
        <div className="col-lg-15">
        
        <p className>© 2019 Nanda Technologie. tout droits reservés | developpé avec le  <i className ="fa fa-heart"  style={{color: "red"}}></i> par R4y
            
        </p>
        </div>
      
      </div>
        
    </div>
  </section>
 </div>
        )
    }
}