import firebase from 'firebase';
import  'firebase/auth';
import 'firebase/firestore';


 export  const config = {
    apiKey: "AIzaSyAwz6nbywqiY0Vsp0dw1haV9U2BpxqPkX8",
    authDomain: "personnel-519a2.firebaseapp.com",
    databaseURL: "https://personnel-519a2.firebaseio.com",
    projectId: "personnel-519a2",
    storageBucket: "personnel-519a2.appspot.com",
    messagingSenderId: "612126445876",
    appId: "1:612126445876:web:9e188da1fd81f71470d561",
    measurementId: "G-5DTZQSLQRG"
  };
firebase.initializeApp(config);
export const auth = firebase.auth();
export const db = firebase.firestore();
