import React from 'react';
import './App.css';
import { BrowserRouter , Route , Switch } from 'react-router-dom';
import { Header } from './Components/Header';
import { home } from './Components/home';
import { Footer } from './Components/Footer';
import { about } from './Components/about';
import { services } from './Components/services';
import { contact } from './Components/contact';

function App() {
  return (
    <BrowserRouter>
  
      <Switch>
        <Route path = '/' component ={  home  } exact />
         
        <Route path = '/about' component = { about }/>

        <Route path = '/services' component = { services }/>

        <Route path = '/contact' component = { contact }/>
        
      </Switch>
      
      
           <home />
           <about />
           <services />
           <contact />
      <Footer />
</BrowserRouter>
 
  );
}

export default App;
